#
# Makefile
#

all: build

build: clean setup html css js resx

setup:
	mkdir -p builds/resources/
	mkdir -p builds/resources/js
	mkdir -p builds/resources/css

html:
	minify index.html > builds/index.html

css:
	compass compile resources/css
	cp -v resources/css/app.css builds/resources/css/
	cp -vr resources/fonts builds/resources/fonts

js:
	minify resources/js/* > builds/resources/js/script.js

resx:
	cp -vr resources/images builds/resources/
	cp -vr resources/favicons/* builds

clean:
	rm -rf builds

deploy: build
	scp -r builds/* tef:/home/evansfamilywebsite/the-evans.family/sumner

deploytest: build
	scp -r builds/* tef:/home/evansfamilywebsite/the-evans.family/sumner/test
