function toggle_cls(cls) {
    var doc_cls = document.body.className;
    var cls_idx = doc_cls.indexOf(' ' + cls);
    if (cls_idx >= 0) {
        document.body.className = doc_cls.slice(0, cls_idx) + doc_cls.slice(cls_idx + cls.length + 1);
        localStorage[cls] = false;
        return false;
    } else {
        document.body.className += ' ' + cls;
        localStorage[cls] = true;
        return true;
    }
}

function toggle_dark() {
    document.getElementById('dark-light-link').innerHTML = toggle_cls('dark') ?
        'Too dark?' : 'Too light?';
}

function toggle_monspace() {
    document.getElementById('monospace-link').innerHTML = toggle_cls('monospace') ?
        'Want sans-serrif?' : 'Want monospace?';
}

function load_settings() {
    // dark-light-link
    if (localStorage['monospace'] === 'true') {
        toggle_monspace();
    }

    if (localStorage['dark'] === 'true') {
         toggle_dark();
    }
}

